#int
#float
#string
#complex numbers
#list
#Tuple
#Dictionary
#set
#Boolean

number=5 #int
print(number,type(number))

value=9.5 #Float
print(value,type(value))

name="Hasa Learning Tech" #String

complex_number=4+3j #Complex

print(complex_number,type(complex_number))

arr=[] #List
print(arr,type(arr))

tps=()  #Tuple
print(tps,type(tps))

dct={} #Dictionary

print(dct,type(dct))

sets=set()  #Set
print(sets,type(sets))

check=True #Boolean
print(check,type(check))








