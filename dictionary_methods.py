names={"friend":"Naga Sai","channel":"Hasa","name":"Tech","friends":[1,2,3,4,5,6,7,8,9,10,11]}

programming={'basic':"c","oops":'python'}

#keys
#print(names.keys())
#values
#print(names.values())
#items
#print(names.items())
#pop
names.pop("friend")

#print(names.pop("python","No Key"))
#popitem

names.popitem()
#print(names)
#update
names.update(programming)
print(names)
#setdefault
names.setdefault("live")
#print(names)
names['live']="6:00"
#print(names)
names.setdefault("siri","bot")
#print(names)
#clear
names.clear()
print(names)







