#Float Data Type

a=5.2564687
print(f"{a} is {type(a)}")

b=5.
print(b)
c=.9
print(c)

d=9.00000
img=5.0+3j

#To check whether it is int or not
print(d.is_integer())
print(a.is_integer())
#TO represent in the form of ratio.
print(a.as_integer_ratio())

#To represent real and imaginary numbers
print(img.real)
print(img.imag)

