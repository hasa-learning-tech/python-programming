a=5 #integer (int)
print(type(a))
print(f'{a} is {type(a)}')

b=9.0 #Float (float)
print(type(b))
print(f'{b} is {type(b)}')

c="Hasa Learning Tech" #String (str)
print(type(c))
print(f'{c} is {type(c)}')

d=3+4j #Complex
print(type(d))
print(f'{d} is {type(d)}')

boolean=True #bool
print(f'{boolean} is {type(boolean)}')

#list, tuple, set, dict
l=[]
print(f'{l} is {type(l)}')

t=()
print(f'{t} is {type(t)}')

dictionary={}
print(f'{dictionary} is {type(dictionary)}')

sets=set()

print(f'{sets} is {type(sets)}')


