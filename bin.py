binary=0b0011
octal=0o2346
hexa=0xFFFF
print(binary)
print(octal)
print(hexa)

bin1=bin(3)
oct1=oct(1254)
hex1=hex(65535)
print(bin1)
print(oct1)
print(hex1)

num1=int('0101',2)
num2=int('1254',8)
num3=int('65535',16)
print(num1)
print(num2)
print(num3)
