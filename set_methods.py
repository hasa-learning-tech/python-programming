set1={1,2,3,4,5,6,7,8,9}
set2={1,2,3,4,5}
set3={10,11,12,13,14,9,2,3,1,5}
#add
set1.add(10)
#print(set1)

#difference
#print(set1.difference(set2))

#print(set1)
#print(set2)

#difference_update

#set1.difference_update(set2)
#print(set2)

#discard
#set1.discard(6)
#print(set1)
#set1.discard(25)
#print(set1)

#intersection
#print(set1.intersection(set2))

#intersection_update
#print(set1)
#set1.intersection_update(set2)
#print(set1)

#issuperset
#print(set1.issuperset(set2))
#print(set2.issuperset(set1))
#issubset
#print(set2.issubset(set1))


#pop
print(set1)
set1.pop()
#print(set1)
#remove
#set1.remove(3)
print(set1)

#symmetric difference
#print(set1.symmetric_difference(set2))
#print(set2)
#symmetric_difference_update'
#set1.symmetric_difference_update(set2)
#print(set1)

#union
set2.union(set3)
print(set2)

#update
set1.update(set3)

print(set1)









